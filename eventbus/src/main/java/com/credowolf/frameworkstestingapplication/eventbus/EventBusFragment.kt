package com.credowolf.frameworkstestingapplication.eventbus

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.event_bus_fragment.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe


class EventBusFragment : Fragment() {

    companion object {
        fun newInstance() = EventBusFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.event_bus_fragment, container, false)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        rvMessages.layoutManager = LinearLayoutManager(activity)
        rvMessages.adapter = MessageAdapter()

        btnSend.setOnClickListener {
            EventBus.getDefault().post(MessageEvent(edMessage.text.toString()))
        edMessage.text.clear()}
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(rvMessages.adapter)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(rvMessages.adapter)
    }
}

class MessageAdapter(private val messageList: ArrayList<MessageEvent> = ArrayList()) :
    RecyclerView.Adapter<MessageAdapter.MessageHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MessageHolder = MessageHolder(TextView(p0.context))

    override fun getItemCount(): Int = messageList.size

    override fun onBindViewHolder(p0: MessageHolder, p1: Int) = p0.bind(messageList[p1])

    @Subscribe
    fun onMessageEvent(event: MessageEvent) {
        messageList.add(event)
        notifyDataSetChanged()
    }


    inner class MessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: MessageEvent) {
            (itemView as TextView).text = message.message
        }
    }

}

data class MessageEvent(val message: String)
