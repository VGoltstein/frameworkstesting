FrameworksTestingApplication

Every submodule provides Fragment and with Koin is added to MainActivity.

Used libraries:
- Koin [https://github.com/InsertKoinIO/koin]
- Coroutines [https://github.com/Kotlin/kotlinx.coroutines]
- EventBus [https://github.com/greenrobot/EventBus]
