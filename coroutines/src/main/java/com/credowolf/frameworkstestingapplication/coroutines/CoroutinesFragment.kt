package com.credowolf.frameworkstestingapplication.coroutines

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.coroutines_fragment.*
import kotlinx.coroutines.*
import kotlinx.android.synthetic.main.coroutines_fragment.btnLoadImage as loadButton
import kotlinx.android.synthetic.main.coroutines_fragment.mCoroutineImage as mImageView


class CoroutinesFragment : Fragment() {

    companion object {
        fun newInstance() = CoroutinesFragment()
    }

    private lateinit var viewModel: CoroutinesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.coroutines_fragment, container, false)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CoroutinesViewModel::class.java)

        loadButton.setOnClickListener {
            mImageView.setImageBitmap(null)
            mLoadProgress.visibility = View.VISIBLE
            viewModel.loadImage { bitmap ->
                mImageView.setImageBitmap(bitmap)
                mLoadProgress.visibility = View.GONE
            }
        }
    }

    private fun loadData() = GlobalScope.launch(Dispatchers.Main) {
        val result1 = GlobalScope.async {
            delay(1000)
            return@async "Hello"
        }.await() // non ui thread, suspend until task is finished
        Log.e("Result1", result1)
        val result2 = GlobalScope.async {
            delay(3000)
            return@async "world"
        }.await()
        Log.e("Result2", result2)
        val result = "$result1 $result2" // ui thread view.showData(result) // ui thread
        Log.e("Result", "results: $result")
    }


}
