package com.credowolf.frameworkstestingapplication.coroutines

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class CoroutinesViewModel : ViewModel() {

    private val images = listOf(
        "https://img.newatlas.com/honda-cb4-interceptor-concept-2.jpg?auto=format%2Ccompress&ch=Width%2CDPR&fit=crop&h=347&q=60&rect=0%2C103%2C1620%2C912&w=616&s=b7bbfee70fc012c9a4c1f7b765181252",
        "https://i.ytimg.com/vi/si9MK6LTYwg/hqdefault.jpg",
        "https://www.cycleworld.com/sites/cycleworld.com/files/styles/1000_1x_/public/images/2017/11/honda-cb4-interceptor-concept-15.jpg?itok=N0gYvztK",
        "https://www.cycleworld.com/sites/cycleworld.com/files/styles/2000_1x_/public/images/2017/11/honda-cb4-concept-hero.jpg?itok=wjUundV9&fc=50,50",
        "https://motobanda.pl/uploads/articles/25/2978/1hysXhondaCB4InterceptorConcept20.jpg"
    )

    fun loadImage(callback: (Bitmap?) -> Unit) {
        GlobalScope.launch(Dispatchers.Main) {
            val index = Random().nextInt(images.size)
            val bitmap = withContext(Dispatchers.Default) { getBitmapFromURL(images[index]) }
            println("Download problem with link $index")
            delay(500)
            callback(bitmap)
        }
    }

    private fun getBitmapFromURL(src: String): Bitmap? =
        try {
            val url = URL(src)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
}
