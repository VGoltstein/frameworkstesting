package com.credowolf.frameworkstestingapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.credowolf.frameworkstestingapplication.coroutines.CoroutinesFragment
import org.koin.android.ext.android.get

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .add(R.id.mFragmentContainer, get() as CoroutinesFragment)
            .commit()

        //Start activity with navigation fragments
//        startActivity(Intent(this, RootActivity::class.java))
    }
}
