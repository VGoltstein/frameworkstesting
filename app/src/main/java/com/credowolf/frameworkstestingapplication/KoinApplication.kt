package com.credowolf.frameworkstestingapplication

import android.app.Application
import com.credowolf.frameworkstestingapplication.coroutines.CoroutinesFragment
import com.credowolf.frameworkstestingapplication.eventbus.EventBusFragment
import org.koin.android.ext.android.startKoin
import org.koin.dsl.module.module

class KoinApplication : Application() {

    // declaration of my module fragments
    private val myModule = module {
        single { CoroutinesFragment() }
        single { EventBusFragment() }
//        single { RootFragment() }
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(myModule))// start Koin!

//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return
//        }
//        LeakCanary.install(this)
    }

}